﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void submitBtn_Click(object sender, EventArgs e)
    {
        string conString = @"Data Source=.\sqlexpress;Initial Catalog=HASC;Integrated Security=True";
        string conString2 = WebConfigurationManager.ConnectionStrings["HASCConnectionString"].ToString();
        string selectQuery = "SELECT * FROM Divisions";
        SqlConnection myConnection = new SqlConnection(conString2);
        SqlConnection mySecondConnection = new SqlConnection(conString2);

        try
        {
            myConnection.Open();
            mySecondConnection.Open();
            SqlCommand myCommand = new SqlCommand(selectQuery, myConnection);
            SqlDataReader myRdr = myCommand.ExecuteReader();
            SqlCommand mySecondCommand = new SqlCommand("DROP * FROM Divisions", myConnection);

            divisionsDDL.Items.Clear();
            while (myRdr.Read())
            {
                int numRows = mySecondCommand.ExecuteNonQuery();
                string name = myRdr["DivisionName"].ToString();
                // display this
                divisionsDDL.Items.Add(name);
            }
        }
        catch (Exception ex)
        {
            errorLbl.Text = "Exception reading database due to: " + ex.Message;
        }
        finally
        {
            myConnection.Close();
            mySecondConnection.Close();
        }
    }
}