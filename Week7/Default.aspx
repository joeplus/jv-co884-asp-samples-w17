﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1" DataTextField="DivisionName" DataValueField="DivisionID">
    </asp:DropDownList>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:HASCConnectionString %>" SelectCommand="SELECT * FROM [Divisions]"></asp:SqlDataSource>
    <asp:ListBox ID="ListBox1" runat="server" DataSourceID="SqlDataSource2" DataTextField="TeamName" DataValueField="TeamID"></asp:ListBox>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:HASCConnectionString %>" DeleteCommand="DELETE FROM [Teams] WHERE [TeamID] = @TeamID" InsertCommand="INSERT INTO [Teams] ([TeamID], [TeamName], [JerseyColour], [DivisionID]) VALUES (@TeamID, @TeamName, @JerseyColour, @DivisionID)" SelectCommand="SELECT * FROM [Teams] WHERE ([DivisionID] = @DivisionID)" UpdateCommand="UPDATE [Teams] SET [TeamName] = @TeamName, [JerseyColour] = @JerseyColour, [DivisionID] = @DivisionID WHERE [TeamID] = @TeamID">
        <DeleteParameters>
            <asp:Parameter Name="TeamID" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="TeamID" Type="Int32" />
            <asp:Parameter Name="TeamName" Type="String" />
            <asp:Parameter Name="JerseyColour" Type="String" />
            <asp:Parameter Name="DivisionID" Type="Int32" />
        </InsertParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="DropDownList2" Name="DivisionID" PropertyName="SelectedValue" Type="Int32" />
        </SelectParameters>
        <UpdateParameters>
            <asp:Parameter Name="TeamName" Type="String" />
            <asp:Parameter Name="JerseyColour" Type="String" />
            <asp:Parameter Name="DivisionID" Type="Int32" />
            <asp:Parameter Name="TeamID" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <asp:DetailsView ID="DetailsView1" runat="server" AllowPaging="True" AutoGenerateRows="False" CellPadding="4" DataKeyNames="TeamID" DataSourceID="SqlDataSource2" ForeColor="#333333" GridLines="None" Height="50px" Width="125px">
        <AlternatingRowStyle BackColor="White" />
        <CommandRowStyle BackColor="#D1DDF1" Font-Bold="True" />
        <EditRowStyle BackColor="#2461BF" />
        <FieldHeaderStyle BackColor="#DEE8F5" Font-Bold="True" />
        <Fields>
            <asp:BoundField DataField="TeamID" HeaderText="TeamID" ReadOnly="True" SortExpression="TeamID" />
            <asp:BoundField DataField="TeamName" HeaderText="TeamName" SortExpression="TeamName" />
            <asp:TemplateField HeaderText="JerseyColour" SortExpression="JerseyColour">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("JerseyColour") %>'></asp:TextBox>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="TextBox1" ErrorMessage="CompareValidator"></asp:CompareValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("JerseyColour") %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("JerseyColour") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="DivisionID" HeaderText="DivisionID" SortExpression="DivisionID" />
            <asp:CommandField ShowEditButton="True" />
        </Fields>
        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#EFF3FB" />
    </asp:DetailsView>
    <br />
    <br />
    <asp:DropDownList ID="divisionsDDL" runat="server">
</asp:DropDownList>
<asp:Button ID="submitBtn" runat="server" OnClick="submitBtn_Click" Text="Submit" />
    <br />
    <asp:Label ID="errorLbl" runat="server" Text="Label"></asp:Label>
</asp:Content>

