﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StandardControls.aspx.cs" Inherits="StandardControls" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Label ID="Label1" runat="server" Text="Singleline (Default):"></asp:Label>
        <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBox1" ErrorMessage="Textbox1 is required!" CssClass="error" Display="Dynamic">Textbox1 is required!</asp:RequiredFieldValidator>
        <br />
        <asp:Label ID="Label2" runat="server" Text="Multiline:"></asp:Label>
        <asp:TextBox ID="TextBox2" runat="server" TextMode="MultiLine"></asp:TextBox>
        <br />
        <asp:Label ID="Label3" runat="server" Text="Password"></asp:Label>
        <asp:TextBox ID="TextBox3" runat="server" TextMode="Password"></asp:TextBox>
        <br />
        <asp:Label ID="Label12" runat="server" Text="Confirm Password:"></asp:Label>
        <asp:TextBox ID="TextBox12" runat="server" TextMode="Password"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="TextBox12" ErrorMessage="RequiredFieldValidator" CssClass="error" Display="Dynamic">This is a required field!</asp:RequiredFieldValidator>
        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="TextBox3" ControlToValidate="TextBox12" ErrorMessage="Passwords don't match!" CssClass="error" Display="Dynamic">Passwords don&#39;t match!</asp:CompareValidator>
        <br />
        <asp:Label ID="Label4" runat="server" Text="Color"></asp:Label>
        <asp:TextBox ID="TextBox4" runat="server" TextMode="Color"></asp:TextBox>
        <br />
        <asp:Label ID="Label5" runat="server" Text="Date"></asp:Label>
        <asp:TextBox ID="TextBox5" runat="server" TextMode="Date"></asp:TextBox>
        <br />
        <asp:Label ID="Label6" runat="server" Text="Email:"></asp:Label>
        <asp:TextBox ID="TextBox6" runat="server" TextMode="Email"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox6" ErrorMessage="RequiredFieldValidator" CssClass="error" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="TextBox6" ErrorMessage="Email format is invalid." ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" CssClass="error" Display="Dynamic">Email format is invalid.</asp:RegularExpressionValidator>
        <br />
        <asp:Label ID="Label7" runat="server" Text="Number:"></asp:Label>
        <asp:TextBox ID="TextBox7" runat="server" TextMode="Number"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBox7" ErrorMessage="RequiredFieldValidator" CssClass="error" Display="Dynamic"></asp:RequiredFieldValidator>
        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="TextBox7" ErrorMessage="Age must be between 0 and 150" MaximumValue="150" MinimumValue="0" Type="Integer" CssClass="error" Display="Dynamic">Age must be between 0 and 150</asp:RangeValidator>
        <br />
        <asp:Label ID="Label8" runat="server" Text="Range:"></asp:Label>
        <asp:TextBox ID="TextBox8" runat="server" TextMode="Range"></asp:TextBox>
        <br />
        <asp:Label ID="Label9" runat="server" Text="Phone:"></asp:Label>
        <asp:TextBox ID="TextBox9" runat="server" TextMode="Phone"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="TextBox9" CssClass="error" Display="Dynamic" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="TextBox9" CssClass="error" Display="Dynamic" ErrorMessage="RegularExpressionValidator" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}"></asp:RegularExpressionValidator>
        <br />
        <asp:Label ID="Label10" runat="server" Text="URL:"></asp:Label>
        <asp:TextBox ID="TextBox10" runat="server" TextMode="Url"></asp:TextBox>
        <br />
        <asp:Label ID="Label11" runat="server" Text="Time:"></asp:Label>
        <asp:TextBox ID="TextBox11" runat="server" TextMode="Time"></asp:TextBox>
    
        <br />
        <asp:Button ID="Button1" runat="server" Text="Submit" />
    
    </div>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" />
    </form>
</body>
</html>
