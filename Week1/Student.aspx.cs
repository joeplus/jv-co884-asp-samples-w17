﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Student : System.Web.UI.Page
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="sender">The originator of this request</param>
    /// <param name="e">The arguments for this request</param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ViewState["Count"] = 0;
            if (Session["Count"] == null)
                Session["Count"] = 0;
            responseLbl.Text = "Welcome to our site!";
        }
        else
        {
            ViewState["Count"] = (int)ViewState["Count"] + 1;
            Session["Count"] = (int)Session["Count"] + 1;
            responseLbl.Text = "Welcome back to our site! This is your ViewState postback count: " +
                ViewState["Count"].ToString() + "  This is your Session postback count: "
                + Session["Count"].ToString();
            responseLbl.Text = $"Welcome back to our site! This is your ViewState postback count: {ViewState["Count"].ToString()} " 
                               + $"  This is your Session postback count: {Session["Count"].ToString()}";             
        }
    }

    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {

    }

    protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {

    }
}