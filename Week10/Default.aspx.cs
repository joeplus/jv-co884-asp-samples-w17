﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        double[] rainFall = { 10, 20, 15, 40, 35, 20, 10, 5, 10, 10, 20, 20 };
        Chart3.DataBindTable(rainFall);
        Chart3.Series[1].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Line;
        Chart3.Titles.Add("Monthly Precipitation");
    }
}