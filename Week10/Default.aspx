﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Chart ID="Chart1" runat="server" DataSourceID="SqlDataSource1" Height="341px" ToolTip="As of Dec. 31, 2016" Width="422px">
            <series>
                <asp:Series BackHatchStyle="BackwardDiagonal" BorderColor="Blue" BorderDashStyle="DashDot" ChartType="Bar" Color="Red" Name="Series1" Palette="Fire" XValueMember="Category" YValueMembers="Products">
                </asp:Series>
            </series>
            <chartareas>
                <asp:ChartArea Name="ChartArea1">
                </asp:ChartArea>
            </chartareas>
            <Titles>
                <asp:Title Font="Microsoft Sans Serif, 14pt, style=Bold" Name="Title1" Text="Number of Products By Category">
                </asp:Title>
            </Titles>
        </asp:Chart>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:NorthwindConnectionString %>" SelectCommand="SELECT * FROM [ProductCounts] ORDER BY [Category]"></asp:SqlDataSource>
    
    </div>
        <asp:Chart ID="Chart2" runat="server" DataSourceID="SqlDataSource1" Height="381px" Width="645px">
            <series>
                <asp:Series BorderWidth="3" ChartType="Pie" Color="Maroon" Font="Microsoft Sans Serif, 10pt, style=Bold" Name="Series1" XValueMember="Category" YValueMembers="Products">
                </asp:Series>
            </series>
            <chartareas>
                <asp:ChartArea Name="ChartArea1">
                </asp:ChartArea>
            </chartareas>
        </asp:Chart>
        <br />
        <asp:Chart ID="Chart3" runat="server">
            <series>
                <asp:Series ChartType="Line" Name="Series1">
                </asp:Series>
            </series>
            <chartareas>
                <asp:ChartArea Name="ChartArea1">
                </asp:ChartArea>
            </chartareas>
        </asp:Chart>
    </form>
</body>
</html>
