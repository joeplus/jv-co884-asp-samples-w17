﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void SubmitBtn_Click(object sender, EventArgs e)
    {
        GreetingService.GreetingServiceSoapClient greetingService = new GreetingService.GreetingServiceSoapClient("GreetingServiceSoap");
        GreetingLbl.Text = greetingService.SendGreeting(NameTB.Text);
        greetingService.Close();

        ConversionService.ConversorSoapClient conversionService = new ConversionService.ConversorSoapClient("ConversorSoap");
        double temp;
        bool result = double.TryParse(TempTB.Text, out temp);
        if (result)
            TempLbl.Text = conversionService.FaC(temp).ToString("N2");
        conversionService.Close();
    }
}