﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Label ID="Label1" runat="server" Text="Name: "></asp:Label>
        <asp:TextBox ID="NameTB" runat="server"></asp:TextBox>
        <asp:Label ID="GreetingLbl" runat="server"></asp:Label>
        <br />
        <asp:Label ID="Label2" runat="server" Text="Temp(F): "></asp:Label>
        <asp:TextBox ID="TempTB" runat="server"></asp:TextBox>
        <asp:Label ID="TempLbl" runat="server"></asp:Label>
        <br />
        <asp:Button ID="SubmitBtn" runat="server" Text="Submit" OnClick="SubmitBtn_Click" />
    
    </div>
    </form>
</body>
</html>
