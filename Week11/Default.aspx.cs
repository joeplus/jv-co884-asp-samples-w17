﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void FrenchBtn_Click(object sender, EventArgs e)
    {
        CultureInfo french = new CultureInfo("fr-CA");
        DateLbl.Text = DateTime.Now.ToString("D", french);
        NumberLbl.Text = (1234.56).ToString("C", french);
        GreetingLbl.Text = Resources.Resource.Greeting;
    }

    protected void EnglishBtn_Click(object sender, EventArgs e)
    {
        CultureInfo english = new CultureInfo("en-CA");
        DateLbl.Text = DateTime.Now.ToString("D", english);
        NumberLbl.Text = (1234.56).ToString("C", english);
        GreetingLbl.Text = Resources.Resource.Greeting;
    }
}