﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" Culture="auto" UICulture="auto"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Label ID="DateLbl" runat="server"></asp:Label>
    
    </div>
        <asp:Label ID="NumberLbl" runat="server"></asp:Label>
        <br />
        <asp:Calendar ID="Calendar1" runat="server"></asp:Calendar>
        <br />
        <asp:Label ID="GreetingLbl" runat="server"></asp:Label>
        <br />
        <asp:Button ID="FrenchBtn" runat="server" OnClick="FrenchBtn_Click" Text="French" />
        <asp:Button ID="EnglishBtn" runat="server" OnClick="EnglishBtn_Click" Text="English" />
    </form>
</body>
</html>
