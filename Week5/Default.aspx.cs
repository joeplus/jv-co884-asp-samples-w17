﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void GridView1_RowUpdated(object sender, GridViewUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            resultsLbl.Text = e.Exception.Message;
            e.ExceptionHandled = true;
        }
    }

    protected void SqlDataSource2_Updating(object sender, SqlDataSourceCommandEventArgs e)
    {
        foreach (SqlParameter param in e.Command.Parameters)
        {
            if (param.Value == null)
            {
                resultsLbl.Text = "All fields are required!";
                e.Cancel = true;
            }
        }
    }

    protected void GridView1_DataBound(object sender, EventArgs e)
    {
        int num = GridView1.Rows.Count;
    }
}
