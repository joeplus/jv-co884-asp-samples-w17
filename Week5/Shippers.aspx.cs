﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Shippers : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnRead_Click(object sender, EventArgs e)
    {
        SqlConnection myCon = new SqlConnection(ConfigurationManager.ConnectionStrings["NorthwindConnectionString"].ToString());

        try
        {
            myCon.Open();
            SqlCommand myCmd = new SqlCommand("SELECT * From Shippers WHERE ShipperID = @ShipperID", myCon);
            myCmd.Parameters.AddWithValue("@ShipperID", idTB.Text);
            SqlDataReader rdr = myCmd.ExecuteReader();
            //int numRows = myCmd.ExecuteNonQuery();

            if (rdr.Read())
            {
                nameTB.Text = rdr["CompanyName"].ToString();
                label3.Text = rdr["Phone"].ToString();
                resultsLbl.Text = "Found record!";
            }
            else
            {
                nameTB.Text = label3.Text = string.Empty;
                resultsLbl.Text = "Did NOT find the specified record!";
            }

        }
        catch (Exception ex)
        {
            resultsLbl.Text = "Failed to read record due to the following exception: " + ex.Message;
        }
        finally
        {
            myCon.Close();
        }
    }
}