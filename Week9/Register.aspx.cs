﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Register : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            lblResult.Text = "";
            lblResult.ForeColor = Color.Black;

            string con_string = WebConfigurationManager.ConnectionStrings["CHDBConnectionString"].ConnectionString;
            SqlConnection con = new SqlConnection(con_string);

            // Create SQL command object
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "InsertUsers";

            // Create SQL command parameters
            cmd.Parameters.Add("@UserName", SqlDbType.VarChar, 50);
            cmd.Parameters["@UserName"].Value = txtUserName.Text.Trim();
            cmd.Parameters.Add("@Email", SqlDbType.VarChar, 50);
            cmd.Parameters["@Email"].Value = txtEmail.Text.Trim();
            cmd.Parameters.Add("@Password", SqlDbType.VarChar, 50);
            cmd.Parameters["@Password"].Value = txtPassword.Text.Trim();

            using (con)
            {
                try
                {
                    con.Open();
                    int results = cmd.ExecuteNonQuery();
                    if (results == 1)
                        lblResult.Text = "User registered successfully.";
                }
                catch (Exception ex)
                {
                    lblResult.Text = ex.Message;
                    lblResult.ForeColor = Color.Red;
                }
            }
        }
    }
}