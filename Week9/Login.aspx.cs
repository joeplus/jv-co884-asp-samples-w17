﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Login1_Authenticate(object sender, AuthenticateEventArgs e)
    {
        String con_string = WebConfigurationManager.ConnectionStrings["CHDBConnectionString"].ConnectionString;
        SqlConnection con = new SqlConnection(con_string);

        // Create SQL command object
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = con;
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = "VerifyUsers";

        // Create SQL command parameters
        cmd.Parameters.Add("@UserName", SqlDbType.VarChar, 50);
        cmd.Parameters["@UserName"].Value = Login1.UserName.Trim();
        cmd.Parameters.Add("@Password", SqlDbType.VarChar, 50);
        cmd.Parameters["@Password"].Value = Login1.Password.Trim();

        try
        {
            using (con)
            {
                con.Open();
                // Since stored procedure returns single row, single column result set, ExecuteScalar can be used
                int result = (int)cmd.ExecuteScalar();
                if (result == 1)
                {
                    e.Authenticated = true;
                }
            }
        }
        catch (Exception ex)
        {
            lblError.Text = ex.Message;
        }
    }
}