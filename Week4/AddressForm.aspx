﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddressForm.aspx.cs" Inherits="AddressForm" %>

<%@ Register src="AddressControl.ascx" tagname="AddressControl" tagprefix="uc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Panel ID="Panel1" runat="server" GroupingText="Shipping Address">
            <uc1:AddressControl ID="AddressControl1" runat="server" />
        </asp:Panel>
    
        <asp:Panel ID="Panel2" runat="server" GroupingText="Billing Address">
            <uc1:AddressControl ID="AddressControl2" runat="server" />
        </asp:Panel>
    </div>
        <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
    </form>
</body>
</html>
