﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        OrderClassesDataContext orderDC = new OrderClassesDataContext();
        var products = from p in orderDC.Products
                       where p.ProductID > 10
                       orderby p.ProductName
                       select new { p.ProductID, p.ProductName };

        var products2 = orderDC.Products
                        .Where(p => p.ProductID > 10)
                        .OrderBy(p => p.ProductName)
                        .Select(p => new { p.ProductID, p.ProductName });

        foreach (var p in products)
            Label1.Text += p.ProductName + ", ";
    }
}